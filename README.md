# macOS

Tested on macOS Catalina 10.15.5.

## Prepare compiler environment

### Install ARM Cross Compiler

Install with brew:

```
brew tap SergioBenitez/osxct
brew install aarch64-none-elf
brew install qemu
```

and

```
brew tap ArmMbed/homebrew-formulae
brew install arm-none-eabi-gcc
```

### Install newer make version

Install with brew:

```sh
brew install make
```

Then add to shell config:

Edit `~/.zprofile`
```sh
PATH="/usr/local/opt/make/libexec/gnubin:$PATH"
```

After adding the shell config, close and reopen the terminal for it to be in effect.

### Install wget

Required for building the boot folder among others...

```
brew install wget
```

### Install pySerial 

Required for the Serial Bootloader.

```sh
pip3 install pyserial
```

## Get started

### Compiling the basics

Then you need to change stuff in Rules.mk, most important the raspberry pi version you are targetting.
I changed mine to 4 on 32bit architecture (aarch64 needs another compiler for macos that I have not yet found).
After doing that, build all project files.

```sh
./makeall clean
./makeall
```

### Installing Serial Bootloader

First, you need a USB serial debug cable. Once acquired, plug it into a usb port and find out what de dev file is for it. Then create a `Config.mk` file in the project root. The file is not uploaded to git, so you need to perform these steps on every computer you are using. Mine has these contents:

```make
SERIALPORT = /dev/tty.usbserial-31170
FLASHBAUD = 921600
USERBAUD = 115200
```

Then build the bootloader. You need to do this after adding the Config.mk file, the BAUDRATE setting is important during build.

```sh
cd boot
make
```

Copy the contents of the boot folder to an SD card. config.txt is only required when targetting aarch64, which I am not.

To use a serial debug cable for logging, you must create a cmdline.txt file in the root of the sd boot volume, with the following contents:

```sh
logdev=ttyS1
```

Put the SD card into your Raspberry Pi and connect the serial debug cable to GPIO 14/15 before powering on.
To clarify: if you have the pi right before you, and the pin header is on the upper side, and the ports are on the lower side, connect the red pin to the upper right first one, the black to the third, fourth white, fifth green. I'm counting from left to right, row by row. So what I call pin 1, 2 3 ,... are all on the first row of pins.

NOTE: My serial cable (PL2303TA USB to TTL Serial `https://www.aliexpress.com/item/32947122999.html?spm=a2g0s.9042311.0.0.27424c4dkWkpoa`) has the folowwing color coding: Red 5V, Black 0V (Ground), White RX, Green TX. Though I suspect my cable's documentation has white and green mixed up.

Power on the pi.

### Make a sample and flash over the serial bootloader

```sh
cd sample/01-gpiosimple
make flash
```

### Monitor serial port

```sh
screen /dev/tty.usbserial-31170 115200 8N1
```

To exit: Control-A followed by Control-\ and y

### Build, flash and monitor

All-in-one command:

```sh
make flash && screen /dev/tty.usbserial-31170 115200 8N1
```