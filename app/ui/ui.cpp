#include "ui.h"

#include "../config.h"
#include <circle/util.h>

#include <circle/bcmpropertytags.h>

CUI *CUI::s_pThis = 0;

static const char FromUI[] = "ui";

CUI::CUI (void)
{
    s_pThis = this;

    CUIStack::SetGetPageHandler(GetPageStub);
    CUIStack::SetActivatePageHandler(ActivatePageStub);
    CUIStack::SetActivatePageByNameHandler(ActivatePageByNameStub);
    CUIStack::SetHidePageHandler(HidePageStub);
}

CUI::~CUI (void)
{
    s_pThis = 0;
}

CUI *CUI::Get (void)
{
	return s_pThis;
}

boolean CUI::Initialize (CScreenDevice *screen)
{
	boolean bOK = TRUE;

    if (bOK)
	{
        bOK = m_Draw.Initialize(screen);
    }

    if (bOK)
	{
        m_Screen = screen;
    }

	return bOK;
}

void CUI::Run (void)
{
    m_Draw.Clear();

    CBcmPropertyTags Tags;
	TPropertyTagSimple TagSimple;
	TagSimple.nValue = 50;
	if (!Tags.GetTag (PROPTAG_SET_BACKLIGHT, &TagSimple, sizeof TagSimple))
	{
        CLogger::Get ()->Write (FromUI, LogNotice, "Can't set touchscreen backlight");
    }

    /*if (p_pActivePage == 0) {
        return;
    }

    p_pActivePage->DrawAll(&m_Draw);*/

    if (p_pPageStack[p_stackIdx] == 0)
    {
        return;
    }

    p_pPageStack[p_stackIdx]->DrawAll(&m_Draw);
}

void CUI::TouchScreenEventHandler (TTouchScreenEvent Event,
				       unsigned nID, unsigned nPosX, unsigned nPosY)
{
	assert (s_pThis != 0);

	/*CString Message;

	switch (Event)
	{
	case TouchScreenEventFingerDown:
		Message.Format ("Finger #%u down at %u / %u", nID+1, nPosX, nPosY);
		break;

	case TouchScreenEventFingerUp:
		Message.Format ("Finger #%u up", nID+1);
		break;

	case TouchScreenEventFingerMove:
		Message.Format ("Finger #%u moved to %u / %u", nID+1, nPosX, nPosY);
		break;

	default:
		assert (0);
		break;
	}

	CLogger::Get ()->Write (FromUI, LogNotice, Message);*/

    //s_pThis->p_pActivePage->TouchScreenEventHandler(Event, nID, nPosX, nPosY);

    if (s_pThis->p_pPageStack[s_pThis->p_stackIdx] == 0)
    {
        return;
    }
    
    s_pThis->p_pPageStack[s_pThis->p_stackIdx]->TouchScreenEventHandler(Event, nID, nPosX, nPosY);
}

void CUI::Update (void)
{
    /*if (p_pActivePage == 0) {
        return;
    }

    p_pActivePage->UpdateAll();*/


    if (p_pPageStack[p_stackIdx] == 0)
    {
        return;
    }

    p_pPageStack[p_stackIdx]->UpdateAll();
}

void CUI::RegisterPage (CUIPage *page)
{
    pages[pageCount++] = page;
}

void *CUI::GetPageStub (char *name)
{
    return (void *)s_pThis->GetPage(name);
}

CUIPage *CUI::GetPage (char *name) 
{
    for (int p=0;p<pageCount;p++) 
    {
        if (strcmp(pages[p]->name, name) == 0) 
        {
            return pages[p];
        }
    }
}

void CUI::ActivatePageStub (void *page, bool overlay)
{
    s_pThis->ActivatePage((CUIPage *)page, overlay);
}

void CUI::ActivatePage (CUIPage *page, bool overlay)
{
    /*if (p_pActivePage == page) {
        return;
    }

    p_pActivePage = page;
    p_pActivePage->DrawAll(&m_Draw);*/

    if (!overlay) 
    {
        p_stackIdx = 0;
        m_Draw.Clear();
    }
    else {
        p_stackIdx++;
    }
    p_pPageStack[p_stackIdx] = page;
    p_pPageStack[p_stackIdx]->DrawAll(&m_Draw);
}

void CUI::ActivatePage (char *name, bool overlay)
{
    //CLogger::Get ()->Write (FromUI, LogNotice, "activate page %s", name);

    for (int p=0;p<pageCount;p++) 
    {
        if (strcmp(pages[p]->name, name) == 0) 
        {
            ActivatePage(pages[p], overlay);
            return;
        }
    }
}

void CUI::ActivatePageByNameStub (char *name, bool overlay)
{
    s_pThis->ActivatePage(name, overlay);
}

void CUI::HidePageStub (void *page)
{
    s_pThis->HidePage((CUIPage *)page);
}

void CUI::HidePage (CUIPage *page)
{
    if (p_pPageStack[p_stackIdx] != page) 
    {
        return;
    }
    if (p_stackIdx > 0) 
    {
        p_pPageStack[p_stackIdx] = 0;
        p_stackIdx--;
    }
    p_pPageStack[p_stackIdx]->DrawAll(&m_Draw);
}