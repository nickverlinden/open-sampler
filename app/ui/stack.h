#ifndef _uistack_h
#define _uistack_h

typedef void *TUIStackGetPageHandler (char *name);
typedef void TUIStackActivatePageHandler (void *page, bool overlay);
typedef void TUIStackActivatePageByNameHandler (char *name, bool overlay);
typedef void TUIStackHidePageHandler (void *page);

class CUIStack
{
public:
    static void *GetPage (char *name);
    static void ActivatePage (void *page, bool overlay);
    static void ActivatePage (char *name, bool overlay);
    static void HidePage (void *page);

    static void SetGetPageHandler(TUIStackGetPageHandler *handler);
    static void SetActivatePageHandler(TUIStackActivatePageHandler *handler);
    static void SetActivatePageByNameHandler(TUIStackActivatePageByNameHandler *handler);
    static void SetHidePageHandler(TUIStackHidePageHandler *handler);

private:
    static TUIStackGetPageHandler *m_pGetPageHandler;
    static TUIStackActivatePageHandler *m_pActivatePageHandler;
    static TUIStackActivatePageByNameHandler *m_pActivatePageByNameHandler;
    static TUIStackHidePageHandler *m_pHidePageHandler;
};

#endif