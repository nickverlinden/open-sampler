#ifndef _draw_h
#define _draw_h

#include "fonts/fonts.h"
#include <circle/screen.h>

typedef struct {
    unsigned int width, height;
} SIZE_T;

class CUIDraw
{
public:
	CUIDraw (void);
	~CUIDraw (void);

    static float Density;
    static bool Inverted;
    static bool GridLines;

    boolean Initialize (CScreenDevice *screen);

    unsigned GetWidth ();
    unsigned GetHeight ();

    void Pixel (unsigned x, unsigned y, bool invert);

    void Line (unsigned x0, unsigned y0, unsigned x1, unsigned y1, bool invert);

    void Rect (unsigned x, unsigned y, unsigned width, unsigned height, bool invert);

    void RectFill (unsigned x, unsigned y, unsigned width, unsigned height, bool invert); 

    void RectRaised (unsigned x, unsigned y, unsigned width, unsigned height, bool invert); 

    SIZE_T GetMapSize (char *map, unsigned char *tileset);
    void Map (char *map, int x, int y, unsigned char *tileset, int spacing, bool invert, bool bgTransparent, bool wrap);

    void Waves (signed short *pcm, int pcmSize, unsigned x, unsigned y, unsigned width, unsigned height, bool invert);

    void Frame (unsigned x, unsigned y, unsigned width, unsigned height, bool invert); 

    void Dither (void);

    void Clear(void);

private:
    void Pixel (unsigned x, unsigned y, TScreenColor clr);

    void Line (unsigned x0, unsigned y0, unsigned x1, unsigned y1, TScreenColor clr);

    CScreenDevice *m_Screen;
};

#endif