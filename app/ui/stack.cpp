#include "stack.h"

TUIStackGetPageHandler *CUIStack::m_pGetPageHandler = 0;
TUIStackActivatePageHandler *CUIStack::m_pActivatePageHandler = 0;
TUIStackActivatePageByNameHandler *CUIStack::m_pActivatePageByNameHandler = 0;
TUIStackHidePageHandler *CUIStack::m_pHidePageHandler = 0;

void *CUIStack::GetPage (char *name)
{
    if (m_pGetPageHandler == 0)
    {
        return 0;
    }
        
    return m_pGetPageHandler(name);
}
void CUIStack::ActivatePage (void *page, bool overlay)
{
    if (m_pActivatePageHandler == 0)
    {
        return;
    }

    m_pActivatePageHandler(page, overlay);
}
void CUIStack::ActivatePage (char *name, bool overlay)
{
    if (m_pActivatePageByNameHandler == 0)
    {
        return;
    }
        
    m_pActivatePageByNameHandler(name, overlay);
}
void CUIStack::HidePage (void *page)
{
    if (m_pHidePageHandler == 0)
    {
        return;
    }

    m_pHidePageHandler(page);
}
void CUIStack::SetGetPageHandler(TUIStackGetPageHandler *handler)
{
    m_pGetPageHandler = handler;
}
void CUIStack::SetActivatePageHandler(TUIStackActivatePageHandler *handler)
{
    m_pActivatePageHandler = handler;
}
void CUIStack::SetActivatePageByNameHandler(TUIStackActivatePageByNameHandler *handler)
{
    m_pActivatePageByNameHandler = handler;
}
void CUIStack::SetHidePageHandler(TUIStackHidePageHandler *handler)
{
    m_pHidePageHandler = handler;
}