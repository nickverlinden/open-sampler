#ifndef _componentcollection_h
#define _componentcollection_h

#include "../config.h"
#include "component.h"

class CUIComponentCollection
{
public:
	CUIComponentCollection (void *page);
	~CUIComponentCollection (void);

    void Add (CUIComponent *component);

    CUIComponent *Get (int idx);

    int count = 0;

    void *page;
private:
    CUIComponent *components[MAX_UI_COMPONENTS];
};

#endif