#include "page.h"
#include "stack.h"

static const char FromUIPage[] = "ui/page";

CUIPage::CUIPage (void)
: components(this)
{
    components.Add(new CUIButton(400 - 88 - 2, 2, 88, 0, "MODE", ModeButtonEvent, this));
}

CUIPage::~CUIPage (void)
{
}

void CUIPage::TouchScreenEventHandler (TTouchScreenEvent Event,
				       unsigned nID, unsigned nPosX, unsigned nPosY)
{
	/*CString Message;

	switch (Event)
	{
	case TouchScreenEventFingerDown:
		Message.Format ("Finger #%u down at %u / %u", nID+1, nPosX, nPosY);
		break;

	case TouchScreenEventFingerUp:
		Message.Format ("Finger #%u up", nID+1);
		break;

	case TouchScreenEventFingerMove:
		Message.Format ("Finger #%u moved to %u / %u", nID+1, nPosX, nPosY);
		break;

	default:
		assert (0);
		break;
	}

	CLogger::Get ()->Write (FromUIPage, LogNotice, Message);*/

    unsigned oNPosX = nPosX;
    unsigned oNPosY = nPosY;

    nPosX /= CUIDraw::Density;
    nPosY /= CUIDraw::Density;

    for (int c=0;c<components.count;c++) {
        CUIComponent *com = components.Get (c);
        if (overlay && com->tag == this)
        {
            continue;
        }

        switch(Event) 
        {
            case TouchScreenEventFingerDown:
            case TouchScreenEventFingerMove:
                if (com->x < nPosX 
                    && (com->x + com->w) > nPosX
                    && com->y < nPosY
                    && (com->y + com->h) > nPosY) {
                        if (fingers[nID] != 0) {
                            if (fingers[nID] != com) {
                                fingers[nID]->TouchScreenEventHandler(TouchScreenEventUnknown, nID, oNPosX, oNPosY);
                                fingers[nID] = com;
                                fingers[nID]->TouchScreenEventHandler(TouchScreenEventFingerDown, nID, oNPosX, oNPosY);
                                return;
                            }
                            fingers[nID]->TouchScreenEventHandler(Event, nID, oNPosX, oNPosY);
                        }
                        else {
                            fingers[nID] = com;
                            //CLogger::Get ()->Write (FromUIPage, LogNotice, "Sending touch event to control: %d", c);
                            com->TouchScreenEventHandler(TouchScreenEventFingerDown, nID, oNPosX, oNPosY);
                        }
                        return;
                }
                break;
            case TouchScreenEventFingerUp:
                if (fingers[nID] != 0) {
                    fingers[nID]->TouchScreenEventHandler(Event, nID, oNPosX, oNPosY);
                    fingers[nID] = 0;
                }
                break;
        }
    }

    // no match, clear events
    if (fingers[nID] != 0) {
        fingers[nID]->TouchScreenEventHandler(TouchScreenEventUnknown, nID, oNPosX, oNPosY);
        fingers[nID] = 0;
    }

    //CLogger::Get ()->Write (FromUIPage, LogNotice, "No control found on these coordinates. %dx%d", nPosX, nPosY);
}

void CUIPage::DrawAll (CUIDraw *m_Draw) 
{
    //CLogger::Get ()->Write (FromUIPage, LogNotice, "draw all");

    Draw(m_Draw);

    for (int c=0;c<components.count;c++) {
        CUIComponent *com = components.Get (c);
        if (overlay && com->tag == this)
        {
            continue;
        }
        com->Draw(m_Draw);
    }

    if (overlay)
    {
        return;
    }

    // draw page header
    int yOff = 28;
    m_Draw->Line(0, yOff, m_Draw->GetWidth(), yOff, false);

    // draw page title
    if (name != "") {
        SIZE_T s = m_Draw->GetMapSize(name, CREAM10X16);
        m_Draw->Map(name, (m_Draw->GetWidth() / 2) - (s.width / 2), m_Draw->GetHeight() - 20, CREAM10X16, 1, false, false, false);
    }
}

void CUIPage::UpdateAll (void) 
{
    //CLogger::Get ()->Write (FromUIPage, LogNotice, "update all");

    for (int c=0;c<components.count;c++) {
        CUIComponent *com = components.Get (c);
        if (overlay && com->tag == this)
        {
            continue;
        }
        com->Update();
    }

    Update();
}

void CUIPage::ModeButtonEvent(CUIButton *Sender, TUIButtonEvent Event) 
{
    switch(Event)
    {
        /*case UIButtonEventDown:
            CLogger::Get ()->Write(FromUIPage, LogNotice, "MODE DOWN");
            break;
        case UIButtonEventUp:
            CLogger::Get ()->Write(FromUIPage, LogNotice, "MODE UP");
            break;*/
        case UIButtonEventClick:
            //CLogger::Get ()->Write(FromUIPage, LogNotice, "MODE CLICK");
            //m_ui->activatePage(pageMode, Sender->parent);
            //Sender->m_Draw->Dither();
            CUIStack::ActivatePage("MODE", true);
            break;
    }
}