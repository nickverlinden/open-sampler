#include "levelmeter.h"

#include "../draw.h"

#include <circle/logger.h>

static const char FromUILevelMeter[] = "ui/components/levelmeter";

CUILevelMeter::CUILevelMeter (int x, int y, int w, int h, float *amp)
{
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
    this->amp = amp;
}

CUILevelMeter::~CUILevelMeter (void)
{
}

void CUILevelMeter::TouchScreenEventHandler (TTouchScreenEvent Event,
				       unsigned nID, unsigned nPosX, unsigned nPosY)
{
	
}

void CUILevelMeter::Draw (CUIDraw *m_Draw)
{
    this->m_Draw = m_Draw;

    //CLogger::Get ()->Write (FromUILevelMeter, LogNotice, "draw");

    m_Draw->Rect(x, y, w, h, false);
    //m_Draw->Map("LEVEL\nsynthology.io", x + w + 2, y, CREAM6X8, 0, false, false, false);
}

void CUILevelMeter::Update (void) 
{
    //CLogger::Get ()->Write (FromUILevelMeter, LogNotice, "update");

    if (lastAmp < amp[0]) {
        lastAmp = amp[0];
    }
    else {
        lastAmp *= holdPeak;
    }

    // draw peak amplitude
    int ew = w - 2;
    int p = ((h - 2) * lastAmp);
    m_Draw->RectFill(x + 1, y + 1, ew, (h - 2) - p, true);
    m_Draw->RectFill(x + 1, y + 1 + (h - 2) - p, ew, p, false);
}