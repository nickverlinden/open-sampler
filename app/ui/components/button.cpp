#include "button.h"

#include "../draw.h"

#include <circle/logger.h>

static const char FromUIButton[] = "ui/components/button";

CUIButton::CUIButton (int x, int y, int w, int h, char *label, TUIButtonEventHandler *pEventHandler, void *tag )
{
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
    this->label = label;

    this->tag = tag;

    //assert (m_pEventHandler == 0);
	m_pEventHandler = pEventHandler;
	//assert (m_pEventHandler != 0);
}

CUIButton::CUIButton (int x, int y, int w, int h, char *label, TUIButtonEventHandler *pEventHandler )
: CUIButton (x, y, w, h, label, pEventHandler, 0 )
{
}

CUIButton::CUIButton (int x, int y, int w, int h, char *label, void *tag )
: CUIButton (x, y, w, h, label, 0, tag )
{
}

CUIButton::CUIButton (int x, int y, int w, int h, char *label )
: CUIButton (x, y, w, h, label, 0, 0 )
{
}

CUIButton::CUIButton (int x, int y, int w, char *label, TUIButtonEventHandler *pEventHandler, void *tag )
: CUIButton (x, y, w, 0, label, pEventHandler, tag )
{
}

CUIButton::CUIButton (int x, int y, int w, char *label, TUIButtonEventHandler *pEventHandler )
: CUIButton (x, y, w, 0, label, pEventHandler, 0 )
{
}

CUIButton::CUIButton (int x, int y, int w, char *label, void *tag)
: CUIButton (x, y, w, 0, label, 0, tag )
{
}

CUIButton::CUIButton (int x, int y, int w, char *label)
: CUIButton (x, y, w, 0, label, 0, 0 )
{
}

CUIButton::CUIButton (int x, int y, char *label, TUIButtonEventHandler *pEventHandler, void *tag )
: CUIButton (x, y, 0, 0, label, pEventHandler, tag )
{
}

CUIButton::CUIButton (int x, int y, char *label, TUIButtonEventHandler *pEventHandler )
: CUIButton (x, y, 0, 0, label, pEventHandler, 0 )
{
}

CUIButton::CUIButton (int x, int y, char *label, void *tag)
: CUIButton (x, y, 0, 0, label, 0, tag )
{
}

CUIButton::CUIButton (int x, int y, char *label)
: CUIButton (x, y, 0, 0, label, 0, 0 )
{
}

CUIButton::~CUIButton (void)
{
}

void CUIButton::TouchScreenEventHandler (TTouchScreenEvent Event,
				       unsigned nID, unsigned nPosX, unsigned nPosY)
{
	//CString Message;

	switch (Event)
	{
	case TouchScreenEventFingerDown:
		//Message.Format ("Name: %s, Finger #%u down at %u / %u", this->label, nID+1, nPosX, nPosY);
        this->on = true;

        if (m_pEventHandler != 0) 
        {
            m_pEventHandler(this, UIButtonEventDown);
        }
		break;

	case TouchScreenEventFingerUp:
		//Message.Format ("Name: %s, Finger #%u up", this->label, nID+1);
        if (this->on && m_pEventHandler != 0) 
        {
            m_pEventHandler(this, UIButtonEventClick);
        }

        this->on = false;

        if (m_pEventHandler != 0) 
        {
            m_pEventHandler(this, UIButtonEventUp);
        }
		break;

	case TouchScreenEventFingerMove:
		//Message.Format ("Name: %s, Finger #%u moved to %u / %u", this->label, nID+1, nPosX, nPosY);
		break;

    case TouchScreenEventUnknown:
		//Message.Format ("Name: %s, Finger #%u moved outside of control", this->label, nID+1);
        this->on = false;

        if (m_pEventHandler != 0) 
        {
            m_pEventHandler(this, UIButtonEventUp);
        }
		break;

	default:
		assert (0);
		break;
	}

    //CLogger::Get ()->Write(FromUIButton, LogNotice, Message);
}

const int pad = 3 * 2; //3 = akai s6000, * 2 for touch
const int rectS = 3;

void CUIButton::Draw (CUIDraw *m_Draw)
{
    this->m_Draw = m_Draw;

    //CLogger::Get ()->Write (FromUIButton, LogNotice, "draw");

    s = m_Draw->GetMapSize(label, CREAM6X8);
    tx = x;
    ty = y;

    if (w==0) {
        w = s.width + rectS + (pad * 2);
    }
    else {
        tx++;
    }
    if (h==0) {
        h = s.height + rectS + (pad * 2);
    }
    else {
        ty++;
    }
    m_Draw->RectRaised(x, y, w, h, false);
    m_Draw->Map(label, x + ((w / 2) - (s.width / 2)), y + ((h / 2) - (s.height / 2)), CREAM6X8, 0, false, false, false);
}

void CUIButton::Update (void) 
{
    if (lastOn == on)
        return;

    lastOn = on;
    //CLogger::Get ()->Write (FromUIButton, LogNotice, "update");
    m_Draw->RectRaised(x, y, w, h, on);
    //m_Draw->Map(label, x + 1 + pad, y + 1 + pad, CREAM6X8, 0, false, false, false);
    m_Draw->Map(label, x + ((w / 2) - (s.width / 2)), y + ((h / 2) - (s.height / 2)), CREAM6X8, 0, on, false, false);

    //m_Draw->Dither();
}