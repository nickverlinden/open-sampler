#ifndef _levelmeter_h
#define _levelmeter_h

#include "../component.h"

class CUILevelMeter : public CUIComponent
{
public:
	CUILevelMeter (int x, int y, int w, int h, float *amp);
	~CUILevelMeter (void);

	void TouchScreenEventHandler (TTouchScreenEvent Event,
					     unsigned nID, unsigned nPosX, unsigned nPosY);

	void Draw (CUIDraw *m_Draw); 
    void Update (void); 

	float *amp;
	const float holdPeak = 0.92;
private:
	float lastAmp = 0;
};

#endif