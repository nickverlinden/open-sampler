#ifndef _button_h
#define _button_h

#include "../component.h"

enum TUIButtonEvent
{
	UIButtonEventDown,
	UIButtonEventUp,
	UIButtonEventClick,
	UIButtonEventUnknown
};

class CUIButton;
typedef void TUIButtonEventHandler (CUIButton *Sender, TUIButtonEvent Event);

class CUIButton : public CUIComponent
{
public:
	CUIButton (int x, int y, int w, int h, char *label, TUIButtonEventHandler *pEventHandler, void *tag );
	CUIButton (int x, int y, int w, int h, char *label, TUIButtonEventHandler *pEventHandler );
	CUIButton (int x, int y, int w, int h, char *label, void *tag);
	CUIButton (int x, int y, int w, int h, char *label);
	CUIButton (int x, int y, int h, char *label, TUIButtonEventHandler *pEventHandler, void *tag );
	CUIButton (int x, int y, int h, char *label, TUIButtonEventHandler *pEventHandler );
	CUIButton (int x, int y, int h, char *label, void *tag);
	CUIButton (int x, int y, int h, char *label);
	CUIButton (int x, int y, char *label, TUIButtonEventHandler *pEventHandler, void *tag );
	CUIButton (int x, int y, char *label, TUIButtonEventHandler *pEventHandler );
	CUIButton (int x, int y, char *label, void *tag);
	CUIButton (int x, int y, char *label);
	~CUIButton (void);

	void TouchScreenEventHandler (TTouchScreenEvent Event,
					     unsigned nID, unsigned nPosX, unsigned nPosY);

	void Draw (CUIDraw *m_Draw); 
    void Update (void); 

	char *label;
	bool on = false;
private:
	bool lastOn = false;
	SIZE_T s;
	int tx = 0;
	int ty = 0;

	TUIButtonEventHandler *m_pEventHandler = 0;
};

#endif