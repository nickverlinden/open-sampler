#ifndef _page_h
#define _page_h

#include "draw.h"
#include "componentcollection.h"
#include "components/button.h"
#include "components/levelmeter.h"

#include <circle/input/touchscreen.h>
#include <circle/logger.h>

class CUIPage
{
public:
	CUIPage (void);
	~CUIPage (void);

	void DrawAll (CUIDraw *m_Draw);

	void UpdateAll (void);

	virtual void Draw (CUIDraw *m_Draw) = 0;

    virtual void Update (void) = 0;

	void TouchScreenEventHandler (TTouchScreenEvent Event,
					     unsigned nID, unsigned nPosX, unsigned nPosY);

	CUIComponentCollection components;

	bool overlay = false;
	char *name = "";
private:
	static void ModeButtonEvent(CUIButton *Sender, TUIButtonEvent Event);

	CUIComponent *fingers[10] = { 0 };

	CUIDraw *m_Draw;
};

#endif