#ifndef _ui_h
#define _ui_h

#include "../config.h"
#include "draw.h"
#include "page.h"
#include "stack.h"

#include <circle/koptions.h>
#include <circle/logger.h>
#include <circle/screen.h>
#include <circle/input/touchscreen.h>
#include <circle/types.h>

class CUI
{
public:
	CUI (void);
	~CUI (void);

    boolean Initialize (CScreenDevice *screen);

	void Run (void);
    void Update (void);
    
    void RegisterPage (CUIPage *page);
    static void *GetPageStub (char *name);
    CUIPage *GetPage (char *name);
    static void ActivatePageStub (void *page, bool overlay);
    void ActivatePage (CUIPage *page, bool overlay);
    void ActivatePage (char *name, bool overlay);
    static void ActivatePageByNameStub (char *name, bool overlay);
    static void HidePageStub (void *page);
    void HidePage (CUIPage *page);

    static CUI *Get (void);

    static void TouchScreenEventHandler (TTouchScreenEvent Event,
					     unsigned nID, unsigned nPosX, unsigned nPosY);

private:
    CKernelOptions		m_Options;
	CScreenDevice *m_Screen;

    CUIDraw m_Draw;

    int pageCount = 0;
    CUIPage *pages[MAX_UI_PAGES];

    int p_stackIdx = 0;
    CUIPage *p_pPageStack[MAX_UI_PAGESTACK] = { 0 };
    //CUIPage *p_pActivePage = 0;

    static CUI *s_pThis;
};

#endif