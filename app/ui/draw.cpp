#include "draw.h"

#include <circle/logger.h>

static const char FromUIDraw[] = "ui/draw";

//TScreenColor slBGColor = COLOR16(0, 0, 0);
//TScreenColor slFGColor = COLOR16(0, 25, 11);

TScreenColor slColor = COLOR16(2, 2, 2);
TScreenColor bgColor = COLOR16(0, 1, 0);
TScreenColor fgColor = COLOR16(0, 31, 17);

// TScreenColor slBGColor = COLOR16(0, 1, 2);
// TScreenColor slFGColor = COLOR16(21, 22, 27); 
// TScreenColor bgColor = COLOR16(7, 8, 11);
// TScreenColor fgColor = COLOR16(28, 29, 31);

typedef struct {
    unsigned char magic[4];     /* magic number is ignored */
    unsigned int version;
    unsigned int headersize;    /* offset of bitmaps in file */
    unsigned int flags;
    unsigned int length;        /* number of tiles */
    unsigned int bytes_per_tile;
    unsigned int height, 
                 width;         /* dimensions of tiles */
} __attribute__((packed)) BGS_T;

CUIDraw::CUIDraw (void)
//:
{
    //s_pThis = this;
}

CUIDraw::~CUIDraw (void)
{
    //s_pThis = 0;
}

float CUIDraw::Density = 2;
bool CUIDraw::Inverted = false;
bool CUIDraw::GridLines = true;

boolean CUIDraw::Initialize (CScreenDevice *screen)
{
	boolean bOK = TRUE;

    if (bOK)
	{
        m_Screen = screen;
    }

	return bOK;
}

void CUIDraw::Clear (void) 
{
	for (int nPosX = 0; nPosX < m_Screen->GetWidth (); nPosX++)
	{
        for (int nPosY = 0; nPosY < m_Screen->GetHeight (); nPosY++)
        {
            Pixel (nPosX, nPosY, bgColor);
        }
	}
}

void CUIDraw::Dither (void) 
{
	for (int nPosX = 0; nPosX < m_Screen->GetWidth (); nPosX++)
	{
        for (int nPosY = 0; nPosY < m_Screen->GetHeight (); nPosY++)
        {
            if ((int(nPosY / Density) % 2 == 0 && int(nPosX / Density) % 2 == 0) || (int(nPosY / Density) % 2 != 0 && int(nPosX / Density) % 2 != 0))
            {
                Pixel (nPosX, nPosY, fgColor);
            }
        }
	}
}

unsigned CUIDraw::GetWidth () 
{
    return m_Screen->GetWidth() / Density;
}
unsigned CUIDraw::GetHeight ()
{
    return m_Screen->GetHeight() / Density;
}

void CUIDraw::Pixel (unsigned x, unsigned y, bool invert)
{
    for (int dx=0;dx<Density;dx++) {
        for (int dy=0;dy<Density;dy++) {
            Pixel((x * Density) + dx, (y * Density) + dy, invert? bgColor : fgColor);
        }
    }
}

void CUIDraw::Pixel (unsigned x, unsigned y, TScreenColor clr)
{
    if (Inverted) {
        if (clr == fgColor) {
            clr = bgColor;
        }
        else if (clr == bgColor) {
            clr = fgColor;
        }
    }
    if (GridLines && ((x % 2 == 1) || (y % 2 == 1))) {
        /*if (clr == fgColor) {
            //clr = slFGColor;
            clr -= slColor;
        }
        else {
            //clr = slBGColor;
            clr += slColor;
        }*/
        clr &= slColor;
    }

    m_Screen->SetPixel(x, y, clr);
}

void CUIDraw::Line (unsigned x0, unsigned y0, unsigned x1, unsigned y1, bool invert) 
{
    for (int dx=0;dx<Density;dx++) {
        for (int dy=0;dy<Density;dy++) {
            Line((x0 * Density) + dx, (y0 * Density) + dy, (x1 * Density) + dx, (y1 * Density) + dy, invert? bgColor : fgColor);
        }
    }
}

void CUIDraw::Line (unsigned x0, unsigned y0, unsigned x1, unsigned y1, TScreenColor clr) 
{
	int dx, dy, p, x, y;
 
    dx=x1-x0;
    dy=y1-y0;

    x=x0;
    y=y0;

    p=2*dy-dx;

    while(x<x1)
    {
        Pixel (x, y, clr);
        if(p>=0)
        {
            y=y+1;
            p=p+2*dy-2*dx;
        }
        else
        {
            p=p+2*dy;
        }
        x=x+1;
    }
    while(y<y1) {
        Pixel (x, y, clr);
        y++;
    }
}

void CUIDraw::Rect (unsigned x, unsigned y, unsigned width, unsigned height, bool invert) 
{
	for (int nPosX = 0; nPosX < (width * Density); nPosX++)
	{
        for (int d = 0; d < Density; d++) 
        {
            Pixel ((x * Density) + nPosX, (y * Density) + d, invert? bgColor : fgColor);
            Pixel ((x * Density) + nPosX, (y * Density) + ((height - 1) * Density) + d, invert? bgColor : fgColor);
        }
	}
    for (int nPosY = 0; nPosY < (height * Density); nPosY++)
    {
        for (int d = 0; d < Density; d++) 
        {
            Pixel ((x * Density) + d, (y * Density) + nPosY, invert? bgColor : fgColor);
            Pixel ((x * Density) + ((width - 1) * Density) + d, (y * Density) + nPosY, invert? bgColor : fgColor);
        }
    }
}
void CUIDraw::RectRaised (unsigned x, unsigned y, unsigned width, unsigned height, bool invert) 
{
    RectFill (x, y, width, height, !invert);

    x *= Density;
    y *= Density;
    width *= Density;
    height *= Density;

    for (int d=0;d<Density;d++) {
        Line (x + (1 * Density), y + d, x + width - (2 * Density), y + d, invert? bgColor : fgColor);
        Line (x + (1 * Density), y + height - (2 * Density) + d, x + width - (2 * Density), y + height - (2 * Density) + d, invert? bgColor : fgColor);
        Line (x + (2 * Density), y + height - (1 * Density) + d, x + width - (1 * Density), y + height - (1 * Density) + d, invert? bgColor : fgColor);
        
        Line (x + d, y + (1 * Density), x + d, y + height - (2 * Density), invert? bgColor : fgColor);
        Line (x + width - (2 * Density) + d, y + (1 * Density), x + width - (2 * Density) + d, y + height - (2 * Density), invert? bgColor : fgColor);
        Line (x + width - (1 * Density) + d, y + (2 * Density), x + width - (1 * Density) + d, y + height - (1 * Density), invert? bgColor : fgColor);
    }
}

void CUIDraw::RectFill (unsigned x, unsigned y, unsigned width, unsigned height, bool invert) 
{
	for (int nPosX = 0; nPosX < (width * Density); nPosX++)
	{
        for (int nPosY = 0; nPosY < (height * Density); nPosY++)
        {
            Pixel ((x * Density) + nPosX, (y * Density) + nPosY, invert? bgColor : fgColor);
        }
    }
}

void CUIDraw::Frame (unsigned x, unsigned y, unsigned width, unsigned height, bool invert) 
{
	RectFill(x, y, width, height, invert ? bgColor : fgColor);
    Rect(x, y, width, height, invert ? fgColor : bgColor);
    Rect(x + 3, y + 3, width - 6, height - 6, invert ? fgColor : bgColor);
}

SIZE_T CUIDraw::GetMapSize (char *map, unsigned char *tileset) 
{
    BGS_T *tiles = (BGS_T*)tileset;

    SIZE_T s;
    s.width = tiles->width;
    s.height = tiles->height;

    int l=1;
    int w=0;
    int maxW = 0;
    while(*map) {
        w++;
        if(*map=='\n') {
            if (s.width*(w-1) > maxW) {
                maxW = s.width*(w-1);
            }
            w=0;
            l++;
        }
        map++;
    }
    if (maxW < w) {
        maxW = w;
    }

    s.width *= maxW;
    s.height *= l;

    return s;
}

void CUIDraw::Map (char *map, int x, int y, unsigned char *tileset, int spacing, bool invert, bool bgTransparent, bool wrap) 
{
    BGS_T *tiles = (BGS_T*)tileset;
    int tileset_headersize     = tiles->headersize;
    int tileset_length         = tiles->length;
    int tileset_width          = tiles->width;
    int tileset_height         = tiles->height;
    int tileset_bytespertile   = tiles->bytes_per_tile;
    int tileset_bytesperline   = (tileset_width+7)/8;

    int x_init  = x;            // initial x coordinate
    int y_init  = y;            // initial y coordinate
    int l       = 0;            // current line relative to starting position           

    while(*map) {
        int i,j,line;

        // get the offset of the tile.
        unsigned char *tile = (unsigned char*)tileset + tileset_headersize + (*((unsigned char*)map)<tileset_length?*map:0) * tileset_bytespertile;

        // handle carrige return
        if(*map=='\r') {
            x = x_init;
        } else
        // handle new line
        if(*map=='\n') {
            x = x_init; 
            l++;
            //rendered_height += tileset_height;
        } else {
            // draw tile
                for(j=0;j<tileset_height;j++){
                    // display one row
                    //line=offs;

                    for(i=0;i<tileset_width+spacing;i++){
                        int bits = i>=tileset_width? 0 : tile[j * tileset_bytesperline + i / 8];
                        int bit = bits >> (7 - i % 8) & 1;
                        if (bit || (!bit && !bgTransparent)) 
                        { 
                            for (int dx=0;dx<Density;dx++) {
                                for (int dy=0;dy<Density;dy++) {
                                    Pixel((x * Density) + (i * Density) + dx, (y * Density) + (j * Density) + (l * (tileset_height * Density)) + dy, bit ? (invert? bgColor : fgColor) : (invert? fgColor : bgColor));
                                }
                            }
                        }

                        //mask>>=1;
                        //line+=4;
                    }
                    // adjust to next tile
                    //offs += pitch;
                }
                x += tileset_width + spacing;
        }
        // next tile
        map++;
    }
}

/*void CUIDraw::Map (char *map, int x, int y, unsigned char *tileset, int spacing, bool invert, bool bgTransparent, bool wrap) 
{
    int fgColor = 255;
    int bgColor = 35;
    int x_init  = x;            // initial x coordinate
    int y_init  = y;            // initial y coordinate
    int l       = 0;            // current line relative to starting position

    CBcmFrameBuffer *buffer = m_Screen->GetFrameBuffer();
    int pitch   = buffer->GetPitch();// / buffer->GetWidth();
    int depth   = buffer->GetDepth();
    int width   = buffer->GetWidth();
    //int height   = buffer->GetHeight();

    //CLogger::Get ()->Write (FromUIDraw, LogNotice, "screenbuffer pitch, depth: %d, %d", pitch, depth);

    BGS_T *tiles = (BGS_T*)tileset;
    int tileset_headersize     = tiles->headersize;
    int tileset_length         = tiles->length;
    int tileset_width          = tiles->width;
    int tileset_height         = tiles->height;
    int tileset_bytespertile   = tiles->bytes_per_tile;
    int tileset_bytesperline   = (tileset_width+7)/8;

    //static struct SIZE_T rendered;
    int rendered_width = 0;
    int rendered_height = tileset_height;

    // draw next tile if it's not zero
    while(*map) {
        // get the offset of the tile.
        unsigned char *tile = (unsigned char*)tileset +
         tileset_headersize + (*((unsigned char*)map)<tileset_length?*map:0) * tileset_bytespertile;
        // calculate the offset on screen
        int offs = ((y_init * pitch) + (l * tileset_height * pitch)) + (x * (depth / 8));
        // variables
        int i,j,line;//,mask; //OLD WAY
        // handle carrige return
        if(*map=='\r') {
            x = x_init;
        } else
        // handle new line
        if(*map=='\n') {
            x = x_init; 
            l++;
            rendered_height += tileset_height;
        } else {
            // handle wrapping and overflow
            if ((x + tileset_width+spacing) > width) {
                rendered_width += tileset_width + spacing;

                if (wrap) {
                    x = x_init; 
                    l++;
                    rendered_height += tileset_height;
                    continue;
                }
            }
            else {
                // draw tile
                for(j=0;j<tileset_height;j++){
                    // display one row
                    line=offs;

                    for(i=0;i<tileset_width+spacing;i++){
                        int bits = i>=tileset_width? 0 : tile[j * tileset_bytesperline + i / 8];
                        int bit = bits >> (7 - i % 8) & 1;
                        if (bit || (!bit && !bgTransparent)) 
                        { 
                            *((unsigned int*)(buffer->GetBuffer() + line)) = bit ? (invert? bgColor : fgColor) : (invert? fgColor : bgColor);
                        }

                        //mask>>=1;
                        line+=(pitch / width);
                    }
                    // adjust to next tile
                    offs += pitch;
                }
                x += tileset_width + spacing;

                rendered_width += tileset_width + spacing;
            }
        }
        // next tile
        map++;
    }

    //return &rendered;
    return;
}*/

void CUIDraw::Waves (signed short *pcm, int pcmSize, unsigned x, unsigned y, unsigned width, unsigned height, bool invert) 
{
    for (int nPosX = 0; nPosX < ((width * Density) - (2 * Density)); nPosX++)
	{
        int window = ((pcmSize / 2) / ((width * Density) - (2 * Density)));
        unsigned int offset = window * nPosX;
        int max = 0;
        int min = 0;
        for (int i=0;i<window;i++) {
            if (pcm[offset + i] > max) {
                max = pcm[offset + i];
            }
            if (pcm[offset + i] < min) {
                min = pcm[offset + i];
            }
        }

        int minY = int(float(float(min + 32767) / 65536) * ((height * Density) - (2 * Density)));
        int maxY = int(float(float(max + 32767) / 65536) * ((height * Density) - (2 * Density)));

        for (int nPosY = minY; nPosY < maxY; nPosY++) {
           Pixel ((x * Density) + (1 * Density) + nPosX, (y * Density) + (1 * Density) + nPosY, invert? bgColor : fgColor);
        }
    }

    // draw cursor
    // int curX = (pos / (sizeof Sound / 2)) * (width - 2);
    // for (int nPosY = 1; nPosY < (height - 2); nPosY++) {
    //     m_Screen->SetPixel (x + 1 + curX, y + 1 + nPosY, fgColor);
    // }
}