#include "componentcollection.h"

#include <circle/logger.h>

static const char FromUIComponentCollection[] = "ui/componentcollection";

CUIComponentCollection::CUIComponentCollection (void *page)
{
    this->page = page;
}

CUIComponentCollection::~CUIComponentCollection (void)
{
}

void CUIComponentCollection::Add (CUIComponent *component) 
{
    components[count++] = component;
    
    component->parent = page;

    //CLogger::Get ()->Write (FromUIComponentCollection, LogNotice, "added component to collection");
}

CUIComponent *CUIComponentCollection::Get (int idx) 
{
    return components[idx];
}