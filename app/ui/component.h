#ifndef _component_h
#define _component_h

#include "draw.h"

#include <circle/input/touchscreen.h>

class CUIComponent
{
public:
	CUIComponent (void);
	~CUIComponent (void);

    virtual void Draw (CUIDraw *m_Draw) = 0;
    virtual void Update (void) = 0;

    virtual void TouchScreenEventHandler (TTouchScreenEvent Event,
					     unsigned nID, unsigned nPosX, unsigned nPosY) = 0;

    CUIDraw *m_Draw;

    int x;
    int y;
    int w;
    int h;

    void *tag;

    void *parent;
};

#endif