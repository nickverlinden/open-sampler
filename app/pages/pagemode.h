#ifndef _pagemode_h
#define _pagemode_h

#include "../ui/page.h"

class CPageMode : public CUIPage
{
public:
	CPageMode (void);
	~CPageMode (void);

	void Draw (CUIDraw *m_Draw); 
    void Update (void); 

private:
	static void ModeButtonEvent(CUIButton *Sender, TUIButtonEvent Event);
};

#endif