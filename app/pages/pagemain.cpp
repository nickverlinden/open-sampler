#include "pagemain.h"

#include "../mixer.h"

static const char FromPageMain[] = "pages/pagemain";

CPageMain::CPageMain ()
{
    name = "MAIN";

    int w = 8;
    int s = 1;
    for (int v=0;v<1;v++) {
        components.Add(new CUILevelMeter(400 - w - 2 + (v * w) + (v * (s + 1)) ,30 , w, 240 - 30 - 20, CMixer::Get ()->GetPeakAmplitude()));
    }
    components.Add(new CUIButton(2, 34, 88, "EDIT", EditButtonEvent));
    components.Add(new CUIButton(2, 60, 88, "SETTINGS"));
    components.Add(new CUIButton(2, 86, 88, "ARM"));
    components.Add(new CUIButton(2, 112, 88, "12345678901234"));
    components.Add(new CUIButton(2, 138, 88, "TRIM"));
    components.Add(new CUIButton(2, 164, 88, "FADE IN"));
    components.Add(new CUIButton(2, 190, 88, "FADE OUT"));
}

CPageMain::~CPageMain (void)
{
}

void CPageMain::Draw (CUIDraw *m_Draw) 
{
    //CLogger::Get ()->Write (FromPageMain, LogNotice, "draw");

    //m_Draw->Clear();
}

void CPageMain::Update (void) 
{
    //CLogger::Get ()->Write (FromPageMain, LogNotice, "update");

    CMixer::Get ()->ResetPeakAmplitude();
}

void CPageMain::EditButtonEvent(CUIButton *Sender, TUIButtonEvent Event) 
{
    switch(Event)
    {
        case UIButtonEventDown:
            CLogger::Get ()->Write(FromPageMain, LogNotice, "EDIT DOWN");
            break;
        case UIButtonEventUp:
            CLogger::Get ()->Write(FromPageMain, LogNotice, "EDIT UP");
            break;
        case UIButtonEventClick:
            CLogger::Get ()->Write(FromPageMain, LogNotice, "EDIT CLICK");
            break;
    }
}