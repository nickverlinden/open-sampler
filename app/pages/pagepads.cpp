#include "pagepads.h"

#include "../mixer.h"
#include "../samplecollection.h"

static const char FromPagePads[] = "pages/pagemain";

CPagePads::CPagePads (void)
{
    name = "PADS";

    int w = 8;
    int s = 1;
    for (int v=0;v<1;v++) {
        components.Add(new CUILevelMeter(400 - w - 2 + (v * w) + (v * (s + 1)) ,30 , w, 240 - 30 - 20, CMixer::Get ()->GetPeakAmplitude()));
    }
    /*int w = 4;
    int s = 1;
    for (int v=0;v<32;v++) {
        components.Add(new CUILevelMeter(400 - w - (2 + (v * w) + (v * (s + 1))) ,34 , w, 240 - 34 - 24, CMixer::Get ()->GetVoicePeakAmplitude(v)));
    }*/
    int pw = 42;
    int ps = 2;
    int x=0;
    for (int p=0;p<16;p++) {
        if (x > 3) {
            x = 0;
        }
        int *t = new int;
        *t = (16 - (4 - x) - (p - x));
        components.Add(new CUIButton(2 + (x++ * (pw + ps)), 34 + ((pw + ps) * (p / 4)), pw, pw, "", PadButtonEvent, t));
    }
}

CPagePads::~CPagePads (void)
{
}

void CPagePads::Draw (CUIDraw *m_Draw) 
{
    //CLogger::Get ()->Write (FromPagePads, LogNotice, "draw");

    //m_Draw->Clear();
}

void CPagePads::Update (void) 
{
    //CLogger::Get ()->Write (FromPagePads, LogNotice, "update");

    CMixer::Get ()->ResetPeakAmplitude();
}

void CPagePads::PadButtonEvent(CUIButton *Sender, TUIButtonEvent Event) 
{
    switch(Event)
    {
        /*case UIButtonEventDown:
            CLogger::Get ()->Write(FromPagePads, LogNotice, "PAD %02d DOWN", *(int *)Sender->tag);
            break;
        case UIButtonEventUp:
            CLogger::Get ()->Write(FromPagePads, LogNotice, "PAD %02d UP", *(int *)Sender->tag);
            break;*/
        case UIButtonEventClick:
            CMixer::Get ()->Play(CSampleCollection::Get ()->Get (*(int *)Sender->tag));
            //CLogger::Get ()->Write(FromPagePads, LogNotice, "PAD %02d CLICK", *(int *)Sender->tag);
            break;
    }
}