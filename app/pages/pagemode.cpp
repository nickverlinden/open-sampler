#include "pagemode.h"

#include "../ui/stack.h"

static const char FromPageMode[] = "pages/pagemode";

CPageMode::CPageMode (void)
{
    name = "MODE";
    overlay = true;

    components.Add(new CUIButton(2, 34, 88, 0, "MAIN", ModeButtonEvent));
    components.Add(new CUIButton(2, 60, 88, 0, "PADS", ModeButtonEvent));
}

CPageMode::~CPageMode (void)
{
}

void CPageMode::Draw (CUIDraw *m_Draw) 
{
    //CLogger::Get ()->Write (FromPagePads, LogNotice, "draw");

    m_Draw->Dither();
}

void CPageMode::Update (void) 
{
    //CLogger::Get ()->Write (FromPagePads, LogNotice, "update");
}

void CPageMode::ModeButtonEvent(CUIButton *Sender, TUIButtonEvent Event) 
{
    switch(Event)
    {
        /*case UIButtonEventDown:
            CLogger::Get ()->Write(FromPagePads, LogNotice, "PAD %02d DOWN", *(int *)Sender->tag);
            break;
        case UIButtonEventUp:
            CLogger::Get ()->Write(FromPagePads, LogNotice, "PAD %02d UP", *(int *)Sender->tag);
            break;*/
        case UIButtonEventClick:
            //if (Sender->label == "PADS")
            //{
                CUIStack::ActivatePage(Sender->label, false);
            //}
            //CMixer::Get ()->Play(CSampleCollection::Get ()->Get (*(int *)Sender->tag));
            //CLogger::Get ()->Write(FromPagePads, LogNotice, "PAD %02d CLICK", *(int *)Sender->tag);
            break;
    }
}