#ifndef _pagepads_h
#define _pagepads_h

#include "../ui/page.h"

class CPagePads : public CUIPage
{
public:
	CPagePads (void);
	~CPagePads (void);

	void Draw (CUIDraw *m_Draw); 
    void Update (void); 

private:
	static void PadButtonEvent(CUIButton *Sender, TUIButtonEvent Event);
};

#endif