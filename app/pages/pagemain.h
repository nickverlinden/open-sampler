#ifndef _pagemain_h
#define _pagemain_h

#include "../ui/page.h"

class CPageMain : public CUIPage
{
public:
	CPageMain (void);
	~CPageMain (void);

	void Draw (CUIDraw *m_Draw); 
    void Update (void); 

private:
	static void EditButtonEvent(CUIButton *Sender, TUIButtonEvent Event);
};

#endif