#include "voice.h"

#include <assert.h>
#include <math.h>
#include "config.h"
#include <circle/util.h>
#include <circle/logger.h>

static const char FromVoice[] = "voice";

CVoice::CVoice ()
{
    pEq.SetFilterType(HS);
    pEq.CalcCoeffs(10, 6000, 1);
}

CVoice::~CVoice (void)
{
}

bool CVoice::SetSample(CSample *sample, bool force)
{
    if (!force && (this->sample != 0 || queueSample != 0)) 
    {
        return false;
    }

    queueSample = sample;

    return true;
}

float CVoice::GetSoundData (void *pBuffer, unsigned nFrames, bool mix)
{
	u8 *pBuffer8 = (u8 *) pBuffer;

	unsigned nSamples = nFrames * WRITE_CHANNELS;

    TYPE nLevel;
    float amp = 0;

	for (unsigned i = 0; i < nSamples;)
	{
        if (queueSample != 0) {
            sPos = 0;
            sample = queueSample;
            queueSample = 0;
        }

        if (sample != 0 && sample->data != 0)
        {
            pitch = sample->pitch;

            // reset/stop loop
            if ((shift && sPos >= sample->size / 2) || (!shift && sPos * pitch >= sample->size / 2)) {
                //DEBUG
                //CLogger::Get ()->Write (FromVoice, LogNotice, "Stop sample %s", sample->name);

                if (sample->loop == true) {
                    sPos = 0;

                    //debug
                    process = !process;
                }
                else {
                    sample = 0;
                }

                //pEq.Reset();
            }
        }

        if (sample != 0 && sample->data != 0)
        {
            //DEBUG
            //if (sPos == 0) {
            //    CLogger::Get ()->Write (FromVoice, LogNotice, "Playing sample %s", sample->name);
            //}

            // 16 bit signed source
            //TYPE nLevel = sample->data[sPos++];
            nLevel = sample->data[int(sPos++ * pitch)] * gain;

            // 8 bit unsigned source
            //TYPE nLevel = (sample->data[sPos++] - 0x80) << 8;
            //TYPE nLevel = ((sample->data[int(sPos++ * pitch)] - 0x80) << 8) * gain;


            // mix test 4
            //
            // if (sPos >= ((sizeof sample->data) / 3) && sPos < sizeof sample->data) {
            // 	nLevel = MixSamples(nLevel, (sample->data[sPos - ((sizeof sample->data) / 3)] - 0x80) << 8);
            // }
            // if (sPos >= ((sizeof sample->data) / 3) * 2) {
            // 	nLevel = MixSamples(nLevel, (sample->data[sPos - (((sizeof sample->data) / 3) * 2)] - 0x80) << 8);
            // }

            if (process) {
                // decimator
                float decimate = 1.0f;         
                if (decimate != 1) {
                    int idx = int(int(sPos * decimate) / decimate);
                    nLevel = sample->data[int(idx * pitch)] * gain;
                }

                // drive
                float depth = 0;
                if (depth > 0) {
                    nLevel = 32767 * (tanh((float(nLevel)/32767)*depth));
                }
                
                // eq
                //nLevel = TYPE(pEq.FilterLeft(float(nLevel) / 32767) * 32767);
                
                
                
                
                
                
                // * (1 / (depth * 0.66));


                // transistor distortion
                /*float drive = 0.5;
                signed amp = nLevel;
                if (amp < 0) {
                    amp = amp + 65535;
                }
                nLevel = nLevel * ((1 - (amp / 32767)) * drive);*/




                // bit crusher
                /*int bits = 9;
                nLevel = nLevel >> bits;
                nLevel = nLevel << bits;*/
            }

            // shift
            //   we could add some sort af crossfading to minimize clicking artifacts
            if (pitch != 1 && shift) {
                int size = 128 * 32; // a very small sample buffer such as 4 will result in a decimation effect too
                int mult = sPos / size;
                int offset = size * mult;
                int idx = int(offset + ((sPos - offset) * pitch));
                nLevel = sample->data[idx] * gain;
            }

            //i++;
            //pBuffer8[i * TYPE_SIZE] = MixSamples(pBuffer8[i * TYPE_SIZE], TYPE_SIZE);
        }
        else {
            nLevel = 0;
        }

        //TYPE oLevel = 0;
        //memcpy (&oLevel, &pBuffer8[i * TYPE_SIZE], TYPE_SIZE);
        //nLevel += oLevel;
        if (mix) {
            //nLevel = MixSamples(oLevel, nLevel);
            nLevel = MixSamples(*((TYPE *)&pBuffer8[i * TYPE_SIZE]), nLevel);
        }

        for (int c=0;c<WRITE_CHANNELS;c++) {
            memcpy (&pBuffer8[i++ * TYPE_SIZE], &nLevel, TYPE_SIZE);
        }
        
/*#if WRITE_CHANNELS == 2
        //i++;
        //pBuffer8[i * TYPE_SIZE] = MixSamples(pBuffer8[i * TYPE_SIZE], TYPE_SIZE);
		memcpy (&pBuffer8[i++ * TYPE_SIZE], &nLevel, TYPE_SIZE);
#endif*/

        if (nLevel < 0) {
            nLevel -= nLevel;
        }
        if (amp < nLevel) {
            amp = nLevel;
        }
	}

    return amp / 32767;
}

TYPE CVoice::MixSamples(TYPE a, TYPE b) {
    //source: http://atastypixel.com/blog/how-to-mix-audio-samples-properly-on-ios/
	return  
			// If both samples are negative, mixed signal must have an amplitude between the lesser of A and B, and the minimum permissible negative amplitude
			a < 0 && b < 0 ?
				((TYPE)a + (TYPE)b) - (((TYPE)a * (TYPE)b)/(-32768)) :

			// If both samples are positive, mixed signal must have an amplitude between the greater of A and B, and the maximum permissible positive amplitude
			( a > 0 && b > 0 ?
				((TYPE)a + (TYPE)b) - (((TYPE)a * (TYPE)b)/32768)

			// If samples are on opposite sides of the 0-crossing, mixed signal should reflect that samples cancel each other out somewhat
			:
				a + b);
}