#ifndef _samplecollection_h
#define _samplecollection_h

#include "config.h"
#include "sample.h"

class CSampleCollection
{
public:
	CSampleCollection (void);
	~CSampleCollection (void);

    void Add (CSample *sample);

    static CSampleCollection *Get (void);
    CSample *Get (int idx);

    int count = 0;
private:
    CSample *samples[MAX_SAMPLES];

    static CSampleCollection *s_pThis;
};

#endif