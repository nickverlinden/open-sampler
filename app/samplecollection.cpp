#include "samplecollection.h"

#include <circle/logger.h>

static const char FromSampleCollection[] = "samplecollection";

CSampleCollection *CSampleCollection::s_pThis = 0;

CSampleCollection::CSampleCollection (void)
{
    s_pThis = this;
}

CSampleCollection::~CSampleCollection (void)
{
}

CSampleCollection *CSampleCollection::Get (void)
{
	return s_pThis;
}

void CSampleCollection::Add (CSample *sample) 
{
    samples[count++] = sample;

    //CLogger::Get ()->Write (FromUIComponentCollection, LogNotice, "added component to collection");
}

CSample *CSampleCollection::Get (int idx) 
{
    return samples[idx];
}

