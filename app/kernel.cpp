//
// kernel.cpp
//
// Circle - A C++ bare metal environment for Raspberry Pi
// Copyright (C) 2014-2017  R. Stange <rsta2@o2online.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "kernel.h"

#include "config.h"
#include <circle/util.h>
#include <assert.h>
#include "sample.h"
#include "sound.h"

static const char FromKernel[] = "kernel";
static const char FromAC[] = "audiocore";
u32 mainLoopCounter = 0;

CCoreTask *CCoreTask::s_pCoreTask = 0;

CCoreTask::CCoreTask(CKernel *pKernel)	:
	CMultiCoreSupport(&pKernel->m_Memory),
	m_pKernel(pKernel)
{
	s_pCoreTask = this;	
}

CCoreTask::~CCoreTask()
{
	m_pKernel = 0;
}

static bool run = true;
static bool audioReady = false;
CTouchScreenDevice *pTouchScreen;

void CCoreTask::Run(unsigned nCore)
{
	// initialize the audio system on the given core
	if (nCore == AUDIO_CORE)
	{
		m_pKernel->m_Logger.Write (FromKernel, LogNotice, "Starting audio mixer on core %d", nCore);
		//runAudioSystem(nCore,true);
		m_pKernel->m_Mixer.Run();
		audioReady = true;
	}

	if (nCore == UI_CORE)
	{
		m_pKernel->m_Logger.Write (FromKernel, LogNotice, "Starting ui on core %d", nCore);
		m_pKernel->m_UI.Run();
		//m_pKernel->m_UI.ActivatePage(&m_pKernel->m_PageMain);
		m_pKernel->m_UI.RegisterPage(&m_pKernel->m_PageMode);
		m_pKernel->m_UI.RegisterPage(&m_pKernel->m_PageMain);
		m_pKernel->m_UI.RegisterPage(&m_pKernel->m_PagePads);
		m_pKernel->m_UI.ActivatePage(&m_pKernel->m_PagePads, false);

		pTouchScreen = (CTouchScreenDevice *) m_pKernel->m_DeviceNameService.GetDevice ("touch1", FALSE);
		if (pTouchScreen == 0)
		{
			m_pKernel->m_Logger.Write (FromKernel, LogPanic, "Touchscreen not found");
		}
		else 
		{
			pTouchScreen->RegisterEventHandler (m_pKernel->m_UI.TouchScreenEventHandler);
		}
	}

	while (run)
	{
		if (nCore == AUDIO_CORE) 
		{
			//m_pKernel->m_Interrupt.DisableIRQ(ARM_IRQ_DMA0+0);
			m_pKernel->m_Mixer.Run();
			//m_pKernel->m_Interrupt.EnableIRQ(ARM_IRQ_DMA0+0);
		}
		if (nCore == UI_CORE)
		{
			static u32 touch_timer = 0;
			static u32 ui_timer = 0;
			u32 now = m_pKernel->m_Timer.GetClockTicks();
			if (now > touch_timer + (1000000/TOUCH_FRAME_RATE))
			{
				touch_timer = now;

				pTouchScreen->Update();
			}
			if (now > ui_timer + (1000000/UI_FRAME_RATE))
			{
				ui_timer = now;

				m_pKernel->m_UI.Update();
			}

			//get serial input
			char k;
			m_pKernel->m_Serial.Read(&k, 1);
			switch(k) {
				case 'q':
					m_pKernel->m_Logger.Write (FromAC, LogNotice, "Exit key received on uart");
					run = false;
			}

			mainLoopCounter++;
		}
	}
}

static bool shown = false;
void CCoreTask::IPIHandler(unsigned nCore, unsigned nIPI)
{
	if (!shown) {
		CLogger::Get ()->Write (FromAC, LogNotice, "Core %d Interrupt %d", nCore, nIPI);
		shown = true;
	}
	if (nCore == AUDIO_CORE &&
		nIPI == IPI_AUDIO_UPDATE)
	{
		
		//AudioSystem::doUpdate();
		//TODO -> NEED TO DO SOMETHING WITH THE MIXER
	}
}

CKernel::CKernel (void)
:	m_Screen (m_Options.GetWidth (), m_Options.GetHeight ()),
	//m_Screen (800*2, 480*2),
	m_Timer (&m_Interrupt),
	m_Logger (m_Options.GetLogLevel (), &m_Timer),
	m_CoreTask(this)
{
	//m_ActLED.Blink (1);	// show we are alive
}

CKernel::~CKernel (void)
{
}

boolean CKernel::Initialize (void)
{
	boolean bOK = TRUE;

	if (bOK)
	{
		bOK = m_Screen.Initialize ();
	}

	if (bOK)
	{
		bOK = m_Serial.Initialize (115200);
	}

	if (bOK)
	{
		CDevice *pTarget = m_DeviceNameService.GetDevice (m_Options.GetLogDevice (), FALSE);
		if (pTarget == 0)
		{
			pTarget = &m_Screen;
		}

		bOK = m_Logger.Initialize (pTarget);
	}

	if (bOK)
	{
		bOK = m_Interrupt.Initialize ();
	}

	if (bOK)
	{
		bOK = m_Timer.Initialize ();
	}

	if (bOK)
	{
		bOK = m_TouchScreen.Initialize ();
	}

	if (bOK)
	{
		bOK = m_UI.Initialize (&m_Screen);
	}

	if (bOK) 
	{
		m_SampleCollection.Add(new CSample("DRM-Test1", Sound, sizeof Sound, 0.5));
		m_SampleCollection.Add(new CSample("DRM-Test2", Sound, sizeof Sound, 0.6));
		m_SampleCollection.Add(new CSample("DRM-Test3", Sound, sizeof Sound, 0.7));
		m_SampleCollection.Add(new CSample("DRM-Test4", Sound, sizeof Sound, 0.8));
		m_SampleCollection.Add(new CSample("DRM-Test5", Sound, sizeof Sound, 0.9));
		m_SampleCollection.Add(new CSample("DRM-Test6", Sound, sizeof Sound, 1.0));
		m_SampleCollection.Add(new CSample("DRM-Test7", Sound, sizeof Sound, 1.1));
		m_SampleCollection.Add(new CSample("DRM-Test8", Sound, sizeof Sound, 1.2));
		m_SampleCollection.Add(new CSample("DRM-Test9", Sound, sizeof Sound, 1.3));
		m_SampleCollection.Add(new CSample("DRM-Test10", Sound, sizeof Sound, 1.4));
		m_SampleCollection.Add(new CSample("DRM-Test11", Sound, sizeof Sound, 1.5));
		m_SampleCollection.Add(new CSample("DRM-Test12", Sound, sizeof Sound, 1.6));
		m_SampleCollection.Add(new CSample("DRM-Test13", Sound, sizeof Sound, 1.7));
		m_SampleCollection.Add(new CSample("DRM-Test14", Sound, sizeof Sound, 1.8));
		m_SampleCollection.Add(new CSample("DRM-Test15", Sound, sizeof Sound, 1.9));
		m_SampleCollection.Add(new CSample("DRM-Test16", Sound, sizeof Sound, 2.0));
	}

	if (bOK)
	{
		bOK = m_Mixer.Initialize ();
	}

	return bOK;
}

TShutdownMode CKernel::Run (void)
{
	m_Logger.Write (FromKernel, LogNotice, "Compile time: " __DATE__ " " __TIME__);

	m_CoreTask.Initialize();
	m_CoreTask.Run(0);

	return ShutdownReboot;
}