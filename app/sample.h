#ifndef _sample_h
#define _sample_h

class CSample
{
public:
	CSample (void);
    CSample (char *name, signed short *data, int size);
    CSample (char *name, signed short *data, int size, float pitch);
	~CSample (void);

    char *name;
    signed short *data;
    int size;
    bool loop = true;
    float pitch = 1.0f;
};

#endif