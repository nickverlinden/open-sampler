#ifndef _mixer_h
#define _mixer_h

#include <circle/soundbasedevice.h>
#include <circle/types.h>

#ifdef USE_VCHIQ_SOUND
	#include <vc4/vchiq/vchiqdevice.h>
#endif

#include "cs42448.h"

#include "config.h"
#include "voice.h"
#include "samplecollection.h"
#include "sample.h"

class CMixer
{
public:
	CMixer (void);
	~CMixer (void);

    static CMixer *Get (void);

    boolean Initialize (void);

	void Run ();
    static void AudioOutputInterrupt (void *pParam);

    float *GetPeakAmplitude ();
    void ResetPeakAmplitude ();

    void Play(CSample *sample);

private:
	void WriteSoundData (unsigned nFrames);

	// void GetSoundData (void *pBuffer, unsigned nFrames);

    int MixSamples(int a, int b);

    CCS42448 m_CS42448;
    unsigned nQueueSizeFrames;

#ifdef USE_VCHIQ_SOUND
	CVCHIQDevice		m_VCHIQ;
#endif
    unsigned short lastAmplitude;
    int voiceCount = MAX_VOICES;
    int nextVoice = 0;

	CSoundBaseDevice	*m_pSound;

    CVoice *voices;

    static CMixer *s_pThis;
};

#endif