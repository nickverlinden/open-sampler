#ifndef _cs42448_h_
#define _cs42448_h_

#include <circle/types.h>
#include <math.h>
#include "wire.h"


#define AUDIO_INJECTOR_OCTO 
	// The only cs42448 device I have is the Audio Injector Octo
	// It uses an additional 5 gpio pins for reset and rate setting.
	// Theoretically this is a generic cs42448 device if OCTO is
	// not defined, but that has not been tested.


class CCS42448
{
public:
	CCS42448();

    static CCS42448 *Get (void);
	
	virtual const char *getName()  { return "cs42448"; }
	
	void volume(float level);
	void volume(int channel, float level);
    
	void inputLevel(float level);
	void inputLevel(int channel, float level);
        
	virtual void start();
		// public until AudioSystem starts it ...

    #ifdef AUDIO_INJECTOR_OCTO
		static void startClock();
	#endif
	
private:
	u8   m_muteMask;

	u8 read(u8 address);
	void write(u32 address, u32 data);
	void write(u32 address, const void *data, u32 len, bool auto_inc=true);

	#ifdef AUDIO_INJECTOR_OCTO
		static void reset();
	#endif
	
    CWire m_Wire;

    static CCS42448 *s_pThis;
};

#endif