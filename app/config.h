//
// config.h
//
// Circle - A C++ bare metal environment for Raspberry Pi
// Copyright (C) 2017  R. Stange <rsta2@o2online.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef _config_h
#define _config_h

#define UI_FRAME_RATE 60

#define TOUCH_FRAME_RATE 1000

#define MAX_UI_COMPONENTS 64    // maximum number of ui components per page
#define MAX_UI_PAGES 32         // maximum number of pages
#define MAX_UI_PAGESTACK 16     // maximum call stack of pages

#define MAX_SAMPLES 512         // maximum amount of samples in memory

#define MAX_VOICES 2         

#define SAMPLE_RATE	44100		// overall system clock - DEFAULT: 44100

#define WRITE_FORMAT	1		// DEFAULT - 1 // 0: 8-bit unsigned, 1: 16-bit signed, 2: 24-bit signed
#define WRITE_CHANNELS	2		// DEFAULT - 2 // 1: Mono, 2: Stereo

#define VOLUME		0.1		// [0.0, 1.0]

#define QUEUE_SIZE_MSECS 10		// size of the sound queue in milliseconds duration - original 100
#define CHUNK_SIZE	256		// number of samples, written to sound device at once - original 2000

#endif
