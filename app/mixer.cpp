#include "mixer.h"

#include "kernel.h"
#include <circle/pwmsoundbasedevice.h>
#include <circle/i2ssoundbasedevice.h>
#include <circle/util.h>
#include <circle/logger.h>
#include <circle/koptions.h>
#include <circle/interrupt.h>
#include <circle/sched/scheduler.h>
#include <assert.h>
#include <math.h>

#include <circle/bcmpropertytags.h>

// #include "sound.h"
// signed short Sound[256];

#ifdef USE_VCHIQ_SOUND
	#include <vc4/sound/vchiqsoundbasedevice.h>
#endif

#if WRITE_FORMAT == 0
	#define FORMAT		SoundFormatUnsigned8
	#define TYPE		u8
	#define TYPE_SIZE	sizeof (u8)
	#define FACTOR		((1 << 7)-1)
	#define NULL_LEVEL	(1 << 7)
#elif WRITE_FORMAT == 1
	#define FORMAT		SoundFormatSigned16
	#define TYPE		s16
	#define TYPE_SIZE	sizeof (s16)
	#define FACTOR		((1 << 15)-1)
	#define NULL_LEVEL	0
#elif WRITE_FORMAT == 2
	#define FORMAT		SoundFormatSigned24
	#define TYPE		s32
	#define TYPE_SIZE	(sizeof (u8)*3)
	#define FACTOR		((1 << 23)-1)
	#define NULL_LEVEL	0
#endif

CMixer *CMixer::s_pThis = 0;

static const char FromMixer[] = "mixer";
static int SoundIDX = 0;

CMixer::CMixer (void)
:	
#ifdef USE_VCHIQ_SOUND
	m_VCHIQ (&m_Memory, CInterruptSystem::Get ()),
#endif
	m_pSound (0)
{
    s_pThis = this;
}

CMixer::~CMixer (void)
{
    s_pThis = 0;
}

CMixer *CMixer::Get (void)
{
	return s_pThis;
}

boolean CMixer::Initialize (void)
{
	boolean bOK = TRUE;

#ifdef USE_VCHIQ_SOUND
	if (bOK)
	{
		bOK = m_VCHIQ.Initialize ();
	}
#endif

	return bOK;
}

float peakAmp[] = { 0, 0 };

float *CMixer::GetPeakAmplitude () {
    return peakAmp;
}

void CMixer::ResetPeakAmplitude () {
    peakAmp[0] = 0;
    peakAmp[1] = 0;
}

void CMixer::Play(CSample *sample) 
{
	// TODO - implement voice stack
	//CLogger::Get ()->Write (FromMixer, LogNotice, "Trigger play *ptr:%d, name:%s, data:%d, size:%d", sample, sample->name, sample->data, sample->size);

	bool found = false;
	bool force = false;
	int busyCount = 0;
	//for (int v=0;v<voiceCount;v++) 
	while(!found)
	{
		if (busyCount > voiceCount) 
		{
			busyCount = 0;
			force = true;
		}
		if (voices[nextVoice + busyCount].SetSample(sample, force) == true) {
			//CLogger::Get ()->Write (FromMixer, LogNotice, "Assigned to voice %d", nextVoice + busyCount);

			nextVoice += busyCount + 1;
			if (nextVoice >= voiceCount) 
			{
				nextVoice = 0;
			}
			found = true;
			break;
		}
		busyCount++;
	}

	//if(found == false) {
	//	CLogger::Get ()->Write (FromMixer, LogNotice, "Can't play sound, no voice available");
	//}
}

static bool audioInit = true;
void CMixer::Run ()
{
	if (audioInit) {
		audioInit = false;
		CLogger::Get ()->Write (FromMixer, LogNotice, "Start mixer");

		// create voices
		//for (int v=0;v<voiceCount;v++) {
		//    voices[v] = new CVoice();
		//}
		voices = new CVoice[voiceCount];

		//TEST
		//voices[0].SetSample(CSampleCollection::Get()->Get(0), true);
		//voices[1].SetSample(CSampleCollection::Get()->Get(1), true);

		//CLogger::Get ()->Write (FromMixer, LogNotice, "%dM memory", "", mem_get_size()/1000000);
		CLogger::Get ()->Write (FromMixer, LogNotice, "created %03d voices", voiceCount);

		// init octo
		m_CS42448.start();
		CLogger::Get ()->Write (FromMixer, LogNotice, "audioinjector octo initialized");

		// select the sound device
		const char *pSoundDevice = CKernelOptions::Get ()->GetSoundDevice ();
		// overwrite for testing:
		pSoundDevice = "sndi2s";
		if (strcmp (pSoundDevice, "sndpwm") == 0)
		{
			m_pSound = new CPWMSoundBaseDevice (CInterruptSystem::Get (), SAMPLE_RATE, CHUNK_SIZE);
		}
		else if (strcmp (pSoundDevice, "sndi2s") == 0)
		{
			m_pSound = new CI2SSoundBaseDevice (CInterruptSystem::Get (), SAMPLE_RATE, CHUNK_SIZE);
		}
		else
		{
	#ifdef USE_VCHIQ_SOUND
			m_pSound = new CVCHIQSoundBaseDevice (&m_VCHIQ, SAMPLE_RATE, CHUNK_SIZE,
						(TVCHIQSoundDestination) CKernelOptions::Get ()->GetSoundOption ());
	#else
			m_pSound = new CPWMSoundBaseDevice (CInterruptSystem::Get (), SAMPLE_RATE, CHUNK_SIZE);
	#endif
		}
		assert (m_pSound != 0);

		// configure sound device
		if (!m_pSound->AllocateQueue (QUEUE_SIZE_MSECS))
		{
			CLogger::Get ()->Write (FromMixer, LogPanic, "Cannot allocate sound queue");
		}

		m_pSound->SetWriteFormat (FORMAT, WRITE_CHANNELS);

		// initially fill the whole queue with data
		nQueueSizeFrames = m_pSound->GetQueueSizeFrames ();

		WriteSoundData (nQueueSizeFrames);

		// start sound device
		if (!m_pSound->Start ())
		{
			CLogger::Get ()->Write (FromMixer, LogPanic, "Cannot start sound device");
		}

		m_CS42448.startClock();
	}

	//for (unsigned nCount = 0; m_pSound->IsActive (); nCount++)
	//{
		CScheduler::Get ()->MsSleep (QUEUE_SIZE_MSECS / 2);

		// fill the whole queue free space with data
		WriteSoundData (nQueueSizeFrames - m_pSound->GetQueueFramesAvail ());
	//}
}
void CMixer::WriteSoundData (unsigned nFrames)
{
	const unsigned nFramesPerWrite = 500;
	u8 Buffer[nFramesPerWrite * WRITE_CHANNELS * TYPE_SIZE];

	memset(&Buffer, 0, sizeof Buffer);

	while (nFrames > 0)
	{
		unsigned nWriteFrames = nFrames < nFramesPerWrite ? nFrames : nFramesPerWrite;

		//memset(&Buffer, 0, sizeof Buffer);

		//GetSoundData (Buffer, nWriteFrames);
        bool mix = false;
        float amp = 0;
        for (int v=0;v<voiceCount;v++) {
			amp = voices[v].GetSoundData (Buffer, nWriteFrames, mix);

            mix = true;
        }

        if (peakAmp[0] < amp) {
            peakAmp[0] = amp;
        }
        if (peakAmp[1] < amp) {
            peakAmp[1] = amp;
        }

		unsigned nWriteBytes = nWriteFrames * WRITE_CHANNELS * TYPE_SIZE;

		int nResult = m_pSound->Write (Buffer, nWriteBytes);
		if (nResult != (int) nWriteBytes)
		{
			CLogger::Get ()->Write (FromMixer, LogError, "Sound data dropped");
            return;
		}

		nFrames -= nWriteFrames;

		CScheduler::Get ()->Yield ();		// ensure the VCHIQ tasks can run
	}
}

int CMixer::MixSamples(int a, int b) {
    //source: http://atastypixel.com/blog/how-to-mix-audio-samples-properly-on-ios/
	return  
			// If both samples are negative, mixed signal must have an amplitude between the lesser of A and B, and the minimum permissible negative amplitude
			a < 0 && b < 0 ?
				((int)a + (int)b) - (((int)a * (int)b)/(-32768)) :

			// If both samples are positive, mixed signal must have an amplitude between the greater of A and B, and the maximum permissible positive amplitude
			( a > 0 && b > 0 ?
				((int)a + (int)b) - (((int)a * (int)b)/32768)

			// If samples are on opposite sides of the 0-crossing, mixed signal should reflect that samples cancel each other out somewhat
			:
				a + b);
}