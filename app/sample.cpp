#include "sample.h"

CSample::CSample(char *name, signed short *data, int size, float pitch) 
{
    if (name == 0) 
    {
        name = new char[16];
    }

    this->name = name;
    this->data = data;
    this->size = size;
    this->pitch = pitch;
}

CSample::CSample(char *name, signed short *data, int size) 
: CSample(name, data, size, 1.0f) 
{
}

CSample::CSample(void) 
: CSample(0, 0, 0)
{
}

CSample::~CSample(void) 
{
}