//
// kernel.h
//
// Circle - A C++ bare metal environment for Raspberry Pi
// Copyright (C) 2014-2017  R. Stange <rsta2@o2online.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef _kernel_h
#define _kernel_h

#include <circle/memory.h>
#include <circle/actled.h>
#include <circle/koptions.h>
#include <circle/devicenameservice.h>
#include <circle/screen.h>
#include <circle/serial.h>
#include <circle/exceptionhandler.h>
#include <circle/interrupt.h>
#include <circle/timer.h>
#include <circle/logger.h>
#include <circle/sched/scheduler.h>
#include <circle/soundbasedevice.h>
#include <circle/input/touchscreen.h>
#include <circle/types.h>
#include <circle/multicore.h>
#include "ui/ui.h"
#include "pages/pagemode.h"
#include "pages/pagemain.h"
#include "pages/pagepads.h"
#include "samplecollection.h"
#include "mixer.h"

#define IPI_AUDIO_UPDATE  11		// first user IPI + 1 (arbitrary upto 30)

#define AUDIO_CORE 1
#define UI_CORE 0

class CKernel;
class CCoreTask 
: public CMultiCoreSupport
{
	public:
		
		CCoreTask(CKernel *pKernel);
		~CCoreTask();
		void Run(unsigned nCore);
		static CCoreTask *Get() {return s_pCoreTask;}
		
		void IPIHandler(unsigned nCore, unsigned nIPI);
				
	private:

		CKernel *m_pKernel;
		static CCoreTask *s_pCoreTask;
};


enum TShutdownMode
{
	ShutdownNone,
	ShutdownHalt,
	ShutdownReboot
};

class CKernel 
{
public:
	CKernel (void);
	~CKernel (void);

	boolean Initialize (void);

	TShutdownMode Run (void);

private:
	friend class CCoreTask;

	// do not change this order
	CMemorySystem		m_Memory;
	CActLED			m_ActLED;
	CKernelOptions		m_Options;
	CDeviceNameService	m_DeviceNameService;
	CScreenDevice		m_Screen;
	CSerialDevice		m_Serial;
	CExceptionHandler	m_ExceptionHandler;
	CInterruptSystem	m_Interrupt;
	CTimer			m_Timer;
	CLogger			m_Logger;
	CScheduler		m_Scheduler;
	CTouchScreenDevice	m_TouchScreen;

	CCoreTask 	m_CoreTask;

	CUI 		m_UI;
	CPageMode 	m_PageMode;
	CPageMain 	m_PageMain;
	CPagePads 	m_PagePads;

	CSampleCollection m_SampleCollection;
	CMixer 		m_Mixer;
};

#endif