#ifndef _voice_h
#define _voice_h

#include <circle/types.h>

#include "sample.h"
#include "parametric.h"

#define TYPE		s16
#define TYPE_SIZE	sizeof (s16)

class CVoice
{
public:
	CVoice ();
	~CVoice (void);

    bool SetSample(CSample *sample, bool force);

    float GetSoundData (void *pBuffer, unsigned nFrames, bool mix);

    TYPE MixSamples(TYPE a, TYPE b);

    unsigned char id;

    CSample *sample = 0;
private:
	int sPos = 0;

    bool process = true;
    float pitch = 1.3;
	float gain = 0.6;
    bool shift = false;

    CSample *queueSample = 0;

    Parametric pEq;
};

#endif